package org.sid.services;

import org.sid.entities.Course;
import org.sid.repositories.CourseRepository;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/courses")
public class CourseService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Course> fetchAll() {
        return CourseRepository.findAll();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Course add(Course course){
        return CourseRepository.add(course);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Course update(@PathParam("id") Integer id, Course course){
        course.setId(id);
        return CourseRepository.update(course);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Course findOne(@PathParam("id") Integer id){
        return CourseRepository.findOne(id);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteOne(@PathParam("id") Integer id){
        CourseRepository.delete(id);
    }
}
