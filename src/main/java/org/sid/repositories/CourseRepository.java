package org.sid.repositories;

import org.sid.entities.Course;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseRepository {
    private static final Map<Integer, Course> COURSE_MAP = new HashMap<>();

    static {
        init();
    }

    private static void init(){
        COURSE_MAP.put(1, new Course(1, "Configure Jersey with annotation"));
        COURSE_MAP.put(2, new Course(2, "Configure Jersey without web.xml"));
    }

    public static List<Course> findAll(){
        return new ArrayList<>(COURSE_MAP.values());
    }

    public static Course add(Course course){
        course.setId(findAll().size() + 1);
        COURSE_MAP.put(course.getId(), course);
        return course;
    }

    public static Course update(Course course){
        COURSE_MAP.put(course.getId(), course);
        return course;
    }

    public static void delete(Integer id){
        COURSE_MAP.remove(id);
    }

    public static Course findOne(Integer id){
        return COURSE_MAP.get(id);
    }
}
